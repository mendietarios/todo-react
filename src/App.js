import React from "react";
import "./App.scss";
import ToDo from "./component/Todo";


function App() {
  
  return (
    <div className="Home">
      <div className="Home-container">
        <h1 className="Home-title">To do App</h1>
        <ToDo/>
      </div>
    </div>
  )
}

export default App;

