import React, { useState } from "react";
import Input from "./Input";
import Button from "./Button";
import { AiFillCarryOut } from "react-icons/ai";

function Form (props) {
  const { addTask } = props;
  const [inputValue, setInputValue] = useState("");
  const onChange = (event) => {
    setInputValue(event.target.value);
  };

  const onClick = (event) => {
    event.preventDefault();
    addTask(inputValue);
    setInputValue("");
  };

  return (
    <form className="Task-form">
      <Input 
        className="Task-input"
        value={inputValue} 
        onChange={onChange} 
        type="text" 
      />
      <Button 
        className="Task-button"
        type="submit" 
        onClick={onClick} >
        {<AiFillCarryOut className="Task-icon"/>}
      </Button>
    </form>
  );
}
export default Form;
