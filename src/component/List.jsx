import React from 'react'
import  TaskItem  from './Item';

const List = (props) => {
  const {list, deleteTask, markAsComplete, setList}= props;
  const ItemList = ()=> {
    return (
      list?.map(item=> <TaskItem 
        item={item} 
        key={item.id} 
        deleteTask={deleteTask}  
        markAsComplete={markAsComplete} 
        list={list} 
        setList={setList} />)
    );
  }

  return (
    <div>
      <h2 className='Home-title'>List Task</h2>
      <ul className='task-containerlist'>
        {ItemList()}
      </ul>
    </div>
  )
}
export default List;
