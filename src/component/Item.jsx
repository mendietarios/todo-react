import React, { useState, useCallback } from "react";
import Input from "./Input";
import Button from "./Button";
import { AiFillEdit, AiFillDelete, AiFillSave } from "react-icons/ai";

const Item = (props) => {
  const { item, deleteTask, markAsComplete, list, setList } = props;
  const [isEdit, setisEdit] = useState(false);
  const [editText, setEditText] = useState("");


  const handleEdit = () => {
    setisEdit(true);
    setEditText(item.text);
  };

  const handleCheck = () => {
    markAsComplete(item.id);
  };

  const handleDelete = () => {
    deleteTask(item.id);
  };

  const editTask = () => {
    setisEdit(false);
    const newList = [...list];
    const index = newList.findIndex((task) => item.id === task.id);
    newList[index].text = editText;
    setList(newList);
    localStorage.setItem("todolist", JSON.stringify(newList));
  };

  const onChange = useCallback((event) => {
    const text = event.target.value;
    
    setEditText(text);
  }, [ ]);

  const renderContent = () => {
    if (isEdit) {
      return (
        <>
          <Input 
            className="Task-input"
            type="text" 
            onChange={onChange} 
            value={editText} 
          />
          <Button 
            type="button" 
            onClick={editTask} >
            <AiFillSave className="Task-icon"/>
          </Button>
        </>
      );
    }

    return (
      <>
        <span className="Task-title">
          {item.text}
        </span>
        <Button 
          className="Task-button"
          type="button" 
          onClick={handleEdit}>
          <AiFillEdit className="Task-icon"/>
        </Button>
      </>
    );
  };

  return (
    <div key={item.id}
      className="Task-container">
      <Input 
        className="Task-input"
        type="checkbox" 
        onChange={handleCheck} 
        checked={item.complete} 
      />
      {renderContent()}
      <Button 
        className="Task-button"
        type="button" 
        onClick={handleDelete} >
        <AiFillDelete className="Task-icon"/>
      </Button>
    </div>
  );
};

export default Item;
