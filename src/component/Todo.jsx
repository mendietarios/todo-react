import React, { useState, useEffect } from "react";
import Form from "./Form";
import List from "./List";
import { v4 as uuidv4 } from 'uuid';

function ToDo() {
  const [todolist, settodolist] = useState([]);

  useEffect(() => {
    const list = JSON.parse(localStorage.getItem("todolist"));
    if (list) {
      settodolist(list);
    }
  }, []);

  const addTask = (value) => {
    const newTask = {
      id: uuidv4(),
      text: value?.trim(),
      complete: false,
    };
    if(!newTask.text){
      alert("EMPTY TASK!!!");
      return;
    } else{
      const list = [...todolist, newTask];
      settodolist(list)
      localStorage.setItem("todolist", JSON.stringify(list));
    }
  };

  const deleteTask = (id) => {
    const deleteList = todolist.filter((todo) => todo.id !== id);
    settodolist(deleteList);
    localStorage.setItem("todolist", JSON.stringify(deleteList));
  };

  const markAsComplete = (id) => {
    const newList = [...todolist];
    const index = newList.findIndex((todo) => todo.id === id);
    const updateMark = newList[index];
    newList.splice(index, 1, {...updateMark, complete: !updateMark.complete});
    settodolist(newList);
    localStorage.setItem("todolist", JSON.stringify(newList));
  }

  return (
    <div>
      <Form 
        addTask={addTask} />
      <List
        list={todolist}
        setList={settodolist}
        deleteTask={deleteTask}
        markAsComplete={markAsComplete}
      />
    </div>  
  );
}

export default ToDo;