import React from "react";

function Button(props) {
  const { onClick, children, type } = props;
  
  return (
    <button 
      className="Task-button"
      type={type} 
      onClick={onClick}>
      {children}
    </button>
  );
}
export default Button;
